﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SupGo.DAL.NetCore.Models;
using SupGo.DAL.NetCore.Services;

namespace SupGo.API.Controllers
{
    [Route("api/[controller]")]
    [Authorize(Policy = "User")]
    [ApiController]
    public class DiscountController : ControllerBase
    {
        /// <summary>
        /// Get all the Discounts
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public List<Discount> Get()
        {
            return DatabaseService<Discount>.GetAll(true).ToList();
        }

        /// <summary>
        /// Get the Discount with the given UUID
        /// </summary>
        /// <param name="uuid"></param>
        /// <returns></returns>
        [HttpGet("{uuid}", Name = "GetDiscount")]
        public Discount Get(string uuid)
        {
            return DatabaseService<Discount>.GetSingle(true, discount => discount.Uuid.Equals(uuid));
        }

        /// <summary>
        /// Apply a filter on the request
        /// </summary>
        /// <param name="uuid"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("filtered-list")]
        public List<Discount> GetFilteredList([FromBody] KeyValuePair<string, object> filter)
        {
            Type type = typeof(Discount);
            PropertyInfo pi = type.GetProperty(filter.Key);
            return DatabaseService<Discount>.GetList(true, a => pi.GetValue(a).Equals(filter.Value)).ToList();
        }

        /// <summary>
        /// Must be Administrator to create a Discount
        /// </summary>
        /// <param name="discount"></param>
        [HttpPost]
        [Authorize(Policy = "Administrator")]
        public string Post([FromBody] Discount discount)
        {
            if (string.IsNullOrEmpty(discount.Uuid))
            {
                discount.Uuid = Guid.NewGuid().ToString();
                DatabaseService<Discount>.Add(discount);
            }
            return discount.Uuid;
        }

        /// <summary>
        /// Must be Administrator to update a Discount
        /// </summary>
        /// <param name="discount"></param>
        [HttpPut]
        [Authorize(Policy = "Administrator")]
        public void Put([FromBody] Discount discount)
        {
            discount.IdDiscount = DatabaseService<Discount>.GetSingle(false, c => c.Uuid.Equals(discount.Uuid)).IdDiscount;
            DatabaseService<Discount>.Update(discount);
        }

        /// <summary>
        /// Must be Administrator to delete a Discount
        /// </summary>
        /// <param name="discount"></param>
        [HttpDelete("{uuid}")]
        [Authorize(Policy = "Administrator")]
        public void Delete(string uuid)
        {
            Discount discount = DatabaseService<Discount>.GetSingle(false, c => c.Uuid.Equals(uuid));
            DatabaseService<Discount>.Remove(discount);
        }
    }
}
