﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SupGo.API.Services;
using SupGo.DAL.NetCore.Models;
using SupGo.DAL.NetCore.Services;

namespace SupGo.API.Controllers
{
    [Route("api/[controller]")]
    [Authorize(Policy = "User")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        /// <summary>
        /// Get all the products
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public List<Product> Get()
        {
            return DatabaseService<Product>.GetAll(true).ToList();
        }

        /// <summary>
        /// Get the product with the given UUID
        /// </summary>
        /// <param name="uuid"></param>
        /// <returns></returns>
        [HttpGet("{uuid}", Name = "GetProduct")]
        public Product Get(string uuid)
        {
            return DatabaseService<Product>.GetSingle(true, p => p.Uuid.Equals(uuid));
        }

        /// <summary>
        /// Apply a filter on the request
        /// </summary>
        /// <param name="uuid"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("filtered-list")]
        public List<Product> GetFilteredList([FromBody] KeyValuePair<string, object> filter)
        {
            Type type = typeof(Product);
            PropertyInfo pi = type.GetProperty(filter.Key);
            return DatabaseService<Product>.GetList(true, a => pi.GetValue(a).Equals(filter.Value)).ToList();
        }

        /// <summary>
        /// Must be Administrator to create a Product
        /// </summary>
        /// <param name="product"></param>
        [HttpPost]
        [Authorize(Policy = "Administrator")]
        public string Post([FromBody] Product product)
        {
            if (string.IsNullOrEmpty(product.Uuid))
            {
                product.Uuid = Guid.NewGuid().ToString();
                DatabaseService<Product>.Add(product);
                StockService.InitializeStock(product);
            }
            return product.Uuid;
        }

        /// <summary>
        /// Must be Administrator to update a Product
        /// </summary>
        /// <param name="uuid"></param>
        /// <param name="product"></param>
        [HttpPut]
        [Authorize(Policy = "Administrator")]
        public void Put([FromBody] Product product)
        {
            product.IdProduct = DatabaseService<Product>.GetSingle(false, c => c.Uuid.Equals(product.Uuid)).IdProduct;
            DatabaseService<Product>.Update(product);
        }

        /// <summary>
        /// Must be Administrator to delete a Product
        /// </summary>
        /// <param name="product"></param>
        [HttpDelete("{uuid}")]
        [Authorize(Policy = "Administrator")]
        public void Delete(string uuid)
        {
            Product product = DatabaseService<Product>.GetSingle(false, c => c.Uuid.Equals(uuid));
            DatabaseService<Product>.Remove(product);
        }
    }
}
