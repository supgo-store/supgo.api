﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SupGo.DAL.NetCore.Models;

namespace SupGo.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Policy = "User")]
    public class RecommendationController : ControllerBase
    {
        /// <summary>
        /// Get the recommendations for a given User
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [HttpGet("{uuid}", Name = "GetRecommandation")]
        public List<Product> Get(string uuid)
        {
            return CustomizedRecommandationAlgorithm.GetRecommendations(uuid);
        }
    }
}