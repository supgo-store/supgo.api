﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SupGo.DAL.NetCore.Models;
using SupGo.DAL.NetCore.Services;

namespace SupGo.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StoreController : ControllerBase
    {
        /// <summary>
        /// Get all the Stores
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public List<Store> Get()
        {
            return DatabaseService<Store>.GetAll(true).ToList();
        }

        /// <summary>
        /// Get the Store with the given UUID
        /// </summary>
        /// <param name="uuid"></param>
        /// <returns></returns>
        [HttpGet("{uuid}", Name = "GetStore")]
        public Store Get(string uuid)
        {
            return DatabaseService<Store>.GetSingle(true, s => s.Uuid.Equals(uuid));
        }

        /// <summary>
        /// Apply a filter on the request
        /// </summary>
        /// <param name="uuid"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("filtered-list")]
        public List<Store> FilteredList([FromBody] KeyValuePair<string, object> filter)
        {
            Type type = typeof(Store);
            PropertyInfo pi = type.GetProperty(filter.Key);
            return DatabaseService<Store>.GetList(true, a => pi.GetValue(a).Equals(filter.Value)).ToList();
        }

        /// <summary>
        /// Must be Administrator to create a Store
        /// </summary>
        /// <param name="store"></param>
        [HttpPost]
        [Authorize(Policy = "Administrator")]
        public string Post([FromBody] Store store)
        {
            if (string.IsNullOrEmpty(store.Uuid))
            {
                store.Uuid = Guid.NewGuid().ToString();
                DatabaseService<Store>.Add(store);
            }
            return store.Uuid;
        }

        /// <summary>
        /// Musr be Administrator the Update a Store
        /// </summary>
        /// <param name="uuid"></param>
        /// <param name="store"></param>
        [HttpPut]
        [Authorize(Policy = "Administrator")]
        public void Put([FromBody] Store store)
        {
            store.IdStore = DatabaseService<Store>.GetSingle(false, c => c.Uuid.Equals(store.Uuid)).IdStore;
            DatabaseService<Store>.Update(store);
        }

        /// <summary>
        /// Must be Administrator to Delete a Store
        /// </summary>
        /// <param name="store"></param>
        [HttpDelete("{uuid}")]
        [Authorize(Policy = "Administrator")]
        public void Delete(string uuid)
        {
            Store store = DatabaseService<Store>.GetSingle(false, c => c.Uuid.Equals(uuid));
            DatabaseService<Store>.Remove(store);
        }
    }
}
