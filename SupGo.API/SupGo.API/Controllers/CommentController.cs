﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SupGo.DAL.NetCore.Models;
using SupGo.DAL.NetCore.Services;

namespace SupGo.API.Controllers
{
    [Route("api/[controller]")]
    [Authorize(Policy = "User")]
    [ApiController]
    public class CommentController : ControllerBase
    {
        /// <summary>
        /// Get all the comment
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public List<Comment> Get()
        {
            return DatabaseService<Comment>.GetAll(true).ToList();
        }

        /// <summary>
        /// Get the comment for a given UUID
        /// </summary>
        /// <param name="uuid"></param>
        /// <returns></returns>
        [HttpGet("{uuid}", Name = "GetComment")]
        public Comment Get(string uuid)
        {
            return DatabaseService<Comment>.GetSingle(true, comment => comment.Uuid.Equals(uuid));
        }

        /// <summary>
        /// Apply a filter on the request
        /// </summary>
        /// <param name="uuid"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("filtered-list")]
        public List<Comment> GetFilteredList([FromBody] KeyValuePair<string, object> filter)
        {
            Type type = typeof(Comment);
            PropertyInfo pi = type.GetProperty(filter.Key);
            return DatabaseService<Comment>.GetList(true, a => pi.GetValue(a).Equals(filter.Value)).ToList();
        }

        /// <summary>
        /// Create a comment
        /// </summary>
        /// <param name="comment"></param>
        [HttpPost]
        public string Post([FromBody] Comment comment)
        {
            if (string.IsNullOrEmpty(comment.Uuid))
            {
                comment.Uuid = Guid.NewGuid().ToString();
                comment.Datetime = DateTime.Now;
                DatabaseService<Comment>.Add(comment);
            }
            return comment.Uuid;
        }

        /// <summary>
        /// Update a comment
        /// </summary>
        /// <param name="id"></param>
        /// <param name="comment"></param>
        [HttpPut]
        public void Put(int id, [FromBody] Comment comment)
        {
            comment.IdComment = DatabaseService<Comment>.GetSingle(false, c => c.Uuid.Equals(comment.Uuid)).IdComment;
            DatabaseService<Comment>.Update(comment);
        }

        /// <summary>
        /// Delete a comment
        /// </summary>
        /// <param name="comment"></param>
        [HttpDelete("{uuid}")]
        public void Delete(string uuid)
        {
            Comment comment = DatabaseService<Comment>.GetSingle(false, c => c.Uuid.Equals(uuid));
            DatabaseService<Comment>.Remove(comment);
        }
    }
}
