﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SupGo.DAL.NetCore.Models;
using SupGo.DAL.NetCore.Services;

namespace SupGo.API.Controllers
{
    [Route("api/[controller]")]
    [Authorize(Policy = "Administrator")]
    [ApiController]
    public class HistoryController : ControllerBase
    {
        /// <summary>
        /// Get all the Histories
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public List<History> Get()
        {
            return DatabaseService<History>.GetAll(true).ToList();
        }

        /// <summary>
        /// Get the History with the given UUID
        /// </summary>
        /// <param name="uuid"></param>
        /// <returns></returns>
        [HttpGet("{uuid}", Name = "GetHistory")]
        public History Get(string uuid)
        {
            return DatabaseService<History>.GetSingle(true, history => history.Uuid.Equals(uuid));
        }

        /// <summary>
        /// Apply a filter on the request
        /// </summary>
        /// <param name="uuid"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("filtered-list")]
        public List<History> GetFilteredList([FromBody] KeyValuePair<string, object> filter)
        {
            Type type = typeof(History);
            PropertyInfo pi = type.GetProperty(filter.Key);
            return DatabaseService<History>.GetList(true, a => pi.GetValue(a).Equals(filter.Value)).ToList();
        }

        /// <summary>
        /// Create an History
        /// </summary>
        /// <param name="history"></param>
        [HttpPost]
        public string Post([FromBody] History history)
        {
            if (string.IsNullOrEmpty(history.Uuid))
            {
                history.Uuid = Guid.NewGuid().ToString();
                history.Datetime = DateTime.Now;
                DatabaseService<History>.Add(history);
            }
            return history.Uuid;
        }

        /// <summary>
        /// Update an History
        /// </summary>
        /// <param name="history"></param>
        [HttpPut]
        public void Put([FromBody] History history)
        {
            history.IdHistory = DatabaseService<History>.GetSingle(false, c => c.Uuid.Equals(history.Uuid)).IdHistory;
            DatabaseService<History>.Update(history);
        }

        /// <summary>
        /// Delete an History
        /// </summary>
        /// <param name="history"></param>
        [HttpDelete("{uuid}")]
        public void Delete(string uuid)
        {
            History history = DatabaseService<History>.GetSingle(false, c => c.Uuid.Equals(uuid));
            DatabaseService<History>.Remove(history);
        }
    }
}