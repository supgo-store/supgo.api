﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SupGo.API.Services;
using SupGo.DAL.NetCore.Models;
using SupGo.DAL.NetCore.Services;

namespace SupGo.API.Controllers
{
    [Route("api/[controller]")]
    [Authorize(Policy = "User")]
    [ApiController]
    public class StockController : ControllerBase
    {
        /// <summary>
        /// Get all the Stocks
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public List<Stock> Get()
        {
            return DatabaseService<Stock>.GetAll(true).ToList();
        }

        /// <summary>
        /// Get the Stock with the given UUID
        /// </summary>
        /// <param name="uuid"></param>
        /// <returns></returns>
        [HttpGet("{uuid}", Name = "GetStock")]
        public Stock Get(string uuid)
        {
            return DatabaseService<Stock>.GetSingle(true, s => s.Uuid.Equals(uuid));
        }

        /// <summary>
        /// Apply a filter on the request
        /// </summary>
        /// <param name="uuid"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("filtered-list")]
        public List<Stock> GetFilteredList([FromBody] KeyValuePair<string, object> filter)
        {
            Type type = typeof(Stock);
            PropertyInfo pi = type.GetProperty(filter.Key);
            return DatabaseService<Stock>.GetList(true, a => pi.GetValue(a).Equals(filter.Value)).ToList();
        }

        /// <summary>
        /// Must be Administrator to create a Stock
        /// </summary>
        /// <param name="stock"></param>
        [HttpPost]
        [Authorize(Policy = "Administrator")]
        public string Post([FromBody] Stock stock)
        {
            if (string.IsNullOrEmpty(stock.Uuid))
            {
                stock.Uuid = Guid.NewGuid().ToString();
                DatabaseService<Stock>.Add(stock);
            }
            return stock.Uuid;
        }

        /// <summary>
        /// Update the Stock
        /// </summary>
        /// <param name="stock"></param>
        [HttpPut]
        public void Put([FromBody] Stock stock)
        {
            stock.IdStock = DatabaseService<Stock>.GetSingle(false, c => c.Uuid.Equals(stock.Uuid)).IdStock;
            DatabaseService<Stock>.Update(stock);
        }

        /// <summary>
        /// Must be an Administrator to delete a Stock
        /// </summary>
        /// <param name="stock"></param>
        [HttpDelete("{uuid}")]
        [Authorize(Policy = "Administrator")]
        public void Delete(string uuid)
        {
            Stock stock = DatabaseService<Stock>.GetSingle(false, c => c.Uuid.Equals(uuid));
            DatabaseService<Stock>.Remove(stock);
        }
    }
}
