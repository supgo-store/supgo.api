﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SupGo.DAL.NetCore.Models;
using SupGo.DAL.NetCore.Services;
using Action = SupGo.DAL.NetCore.Models.Action;

namespace SupGo.API.Controllers
{
    [Route("api/[controller]")]
    [Authorize(Policy = "User")]
    [ApiController]
    public class ActionController : ControllerBase
    {
        /// <summary>
        /// Get all the Actions
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public List<Action> Get()
        {
            return DatabaseService<Action>.GetAll(true).ToList();
        }

        /// <summary>
        /// Get the Action with the given UUID
        /// </summary>
        /// <param name="uuid"></param>
        /// <returns></returns>
        [HttpGet("{uuid}", Name = "GetAction")]
        public Action Get(string uuid)
        {
            return DatabaseService<Action>.GetSingle(true, a => a.Uuid.Equals(uuid));
        }

        /// <summary>
        /// Apply a filter on the request
        /// </summary>
        /// <param name="uuid"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("filtered-list")]
        public List<Action> GetFilteredList([FromBody] KeyValuePair<string, object> filter)
        {
            Type type = typeof(Action);
            PropertyInfo pi = type.GetProperty(filter.Key);
            return DatabaseService<Action>.GetList(true, a => pi.GetValue(a).Equals(filter.Value)).ToList();
        }

        /// <summary>
        /// Must be an Administrator to create an Action
        /// </summary>
        /// <param name="action"></param>
        [HttpPost]
        [Authorize(Policy = "Administrator")]
        public string Post([FromBody] Action action)
        {
            if (string.IsNullOrEmpty(action.Uuid))
            {
                action.Uuid = Guid.NewGuid().ToString();
                DatabaseService<Action>.Add(action);
            }
            return action.Uuid;
        }

        /// <summary>
        /// Must be an Administrator to update an Action
        /// </summary>
        /// <param name="action"></param>
        [HttpPut]
        [Authorize(Policy = "Administrator")]
        public void Put([FromBody] Action action)
        {
            action.IdAction = DatabaseService<Action>.GetSingle(false, c => c.Uuid.Equals(action.Uuid)).IdAction;
            DatabaseService<Action>.Update(action);
        }

        /// <summary>
        /// Must be an Administrator to delete an Action
        /// </summary>
        /// <param name="action"></param>
        [HttpDelete("{uuid}")]
        [Authorize(Policy = "Administrator")]
        public void Delete(string uuid)
        {
            Action action = DatabaseService<Action>.GetSingle(false, a => a.Uuid.Equals(uuid));
            DatabaseService<Action>.Remove(action);
        }
    }
}
