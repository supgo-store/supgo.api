﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web.Http;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SupGo.DAL.NetCore.Models;
using SupGo.DAL.NetCore.Services;
using AuthorizeAttribute = Microsoft.AspNetCore.Authorization.AuthorizeAttribute;
using FromBodyAttribute = Microsoft.AspNetCore.Mvc.FromBodyAttribute;
using HttpGetAttribute = Microsoft.AspNetCore.Mvc.HttpGetAttribute;
using HttpPostAttribute = Microsoft.AspNetCore.Mvc.HttpPostAttribute;
using HttpPutAttribute = Microsoft.AspNetCore.Mvc.HttpPutAttribute;
using RouteAttribute = Microsoft.AspNetCore.Mvc.RouteAttribute;

namespace SupGo.API.Controllers
{
    [Authorize(Policy = "User")]
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        /// <summary>
        /// Must be Administrator to get all the Users
        /// </summary>
        /// <returns></returns>
        [Authorize(Policy = "Administrator")]
        [HttpGet]
        public List<User> Get()
        {
            List<User> users = DatabaseService<User>.GetAll(true).ToList();
            foreach(User user in users)
            {
                user.CardNumber = null;
                user.CardOwner = null;
                user.CardExpireDate = null;
                user.CardCcv = null;
            }
            return users;
        }

        /// <summary>
        /// Get the User with the given UUID
        /// </summary>
        /// <param name="uuid"></param>
        /// <returns></returns>
        [HttpGet("{uuid}", Name = "GetUser")]
        public User Get(string uuid)
        {
            if(uuid == HttpContext.User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value)
            {
                return DatabaseService<User>.GetSingle(true, u => u.Uuid.Equals(uuid));
            } else
            {
                throw new HttpResponseException(HttpStatusCode.Unauthorized);
            }
        }

        /// <summary>
        /// Apply a filter on the request
        /// </summary>
        /// <param name="uuid"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("filtered-list")]
        [Authorize(Policy = "Administrator")]
        public List<User> GetFilteredList([FromBody] KeyValuePair<string, object> filter)
        {
            Type type = typeof(User);
            PropertyInfo pi = type.GetProperty(filter.Key);
            List<User> users = DatabaseService<User>.GetList(true, a => pi.GetValue(a).Equals(filter.Value)).ToList();

            foreach (User user in users)
            {
                user.CardNumber = null;
                user.CardOwner = null;
                user.CardExpireDate = null;
                user.CardCcv = null;
            }
            return users;
        }

        [HttpPost]
        [Authorize(Policy = "Administrator")]
        public string Post(User user)
        {
            user.Uuid = Guid.NewGuid().ToString();
            DatabaseService<User>.Add(user);
            return user.Uuid;
        }

        /// <summary>
        /// A user can only update itself
        /// </summary>
        /// <param name="category"></param>
        [HttpPut]
        public void Put([FromBody] User user)
        {
            if (user.Uuid == HttpContext.User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value)
            {
                user.IdUser = DatabaseService<User>.GetSingle(false, u => u.Uuid.Equals(user.Uuid)).IdUser;
                DatabaseService<User>.Update(user);
            } else
            {
                throw new HttpResponseException(HttpStatusCode.Unauthorized);
            }
        }
    }
}