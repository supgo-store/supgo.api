﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SupGo.DAL.NetCore.Models;
using SupGo.DAL.NetCore.Services;

namespace SupGo.API.Controllers
{
    [Route("api/[controller]")]
    [Authorize(Policy = "User")]
    [ApiController]
    public class CartController : ControllerBase
    {
        /// <summary>
        /// Get all the Carts
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public List<Cart> Get()
        {
            return DatabaseService<Cart>.GetAll(true).ToList();
        }

        /// <summary>
        /// Get the cart with the given UUID
        /// </summary>
        /// <param name="uuid"></param>
        /// <returns></returns>
        [HttpGet("{uuid}", Name = "GetCart")]
        public Cart Get(string uuid)
        {
            return DatabaseService<Cart>.GetSingle(true, c => c.Uuid.Equals(uuid));
        }

        /// <summary>
        /// Apply a filter on the request
        /// </summary>
        /// <param name="uuid"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("filtered-list")]
        public List<Cart> GetFilteredList([FromBody] KeyValuePair<string, object> filter)
        {
            Type type = typeof(Cart);
            PropertyInfo pi = type.GetProperty(filter.Key);
            return DatabaseService<Cart>.GetList(true, a => pi.GetValue(a).Equals(filter.Value)).ToList();
        }

        /// <summary>
        /// Create a cart
        /// </summary>
        /// <param name="cart"></param>
        [HttpPost]
        public string Post([FromBody] Cart cart)
        {
            if (string.IsNullOrEmpty(cart.Uuid))
            {
                cart.Uuid = Guid.NewGuid().ToString();
                DatabaseService<Cart>.Add(cart);
            }
            return cart.Uuid;
        }

        /// <summary>
        /// Update a cart
        /// </summary>
        /// <param name="cart"></param>
        [HttpPut]
        public void Put([FromBody] Cart cart)
        {
            cart.IdCart = DatabaseService<Cart>.GetSingle(false, c => c.Uuid.Equals(cart.Uuid)).IdCart;
            DatabaseService<Cart>.Update(cart);
        }

        /// <summary>
        /// Delete a cart
        /// </summary>
        /// <param name="cart"></param>
        [HttpDelete("{uuid}")]
        public void Delete(string uuid)
        {
            Cart cart = DatabaseService<Cart>.GetSingle(false, c => c.Uuid.Equals(uuid));
            DatabaseService<Cart>.Remove(cart);
        }
    }
}
