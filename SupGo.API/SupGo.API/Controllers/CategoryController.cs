﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SupGo.DAL.NetCore.Models;
using SupGo.DAL.NetCore.Services;

namespace SupGo.API.Controllers
{
    [Route("api/[controller]")]
    [Authorize(Policy = "User")]
    [ApiController]
    public class CategoryController : ControllerBase
    {
        /// <summary>
        /// Get all the Categories
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public List<Category> Get()
        {
            return DatabaseService<Category>.GetAll(true).ToList();
        }

        /// <summary>
        /// Get the Category with the given UUID
        /// </summary>
        /// <param name="uuid"></param>
        /// <returns></returns>
        [HttpGet("{uuid}", Name="GetCategory")]
        public Category Get(string uuid)
        {
            return DatabaseService<Category>.GetSingle(true, c => c.Uuid.Equals(uuid));
        }

        /// <summary>
        /// Apply a filter on the request
        /// </summary>
        /// <param name="uuid"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("filtered-list")]
        public List<Category> GetFilteredList([FromBody] KeyValuePair<string, object> filter)
        {
            Type type = typeof(Category);
            PropertyInfo pi = type.GetProperty(filter.Key);
            return DatabaseService<Category>.GetList(true, c => pi.GetValue(c).Equals(filter.Value)).ToList();
        }

        /// <summary>
        /// Must be Administrator to create a Category
        /// </summary>
        /// <param name="category"></param>
        [HttpPost]
        [Authorize(Policy = "Administrator")]
        public string Post([FromBody] Category category)
        {
            if(string.IsNullOrEmpty(category.Uuid))
            {
                category.Uuid = Guid.NewGuid().ToString();
                DatabaseService<Category>.Add(category);
            }
            return category.Uuid;
        }

        /// <summary>
        /// Must be Administrator to update a Category
        /// </summary>
        /// <param name="category"></param>
        [HttpPut]
        [Authorize(Policy = "Administrator")]
        public void Put([FromBody] Category category)
        {
            category.IdCategory = DatabaseService<Category>.GetSingle(false, c => c.Uuid.Equals(category.Uuid)).IdCategory;
            DatabaseService<Category>.Update(category);
        }

        /// <summary>
        /// Must be Administrator to delete a Category
        /// </summary>
        /// <param name="uuid"></param>
        [HttpDelete("{uuid}")]
        [Authorize(Policy = "Administrator")]
        public void Delete(string uuid)
        {
            Category category = DatabaseService<Category>.GetSingle(false, c => c.Uuid.Equals(uuid));
            DatabaseService<Category>.Remove(category);
        }
    }
}
