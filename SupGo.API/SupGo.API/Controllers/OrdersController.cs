﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SupGo.DAL.NetCore.Models;
using SupGo.DAL.NetCore.Services;

namespace SupGo.API.Controllers
{
    [Route("api/[controller]")]
    [Authorize(Policy = "User")]
    [ApiController]
    public class OrdersController : ControllerBase
    {
        /// <summary>
        /// Get all the Orders
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public List<Orders> Get()
        {
            return DatabaseService<Orders>.GetAll(true).ToList();
        }

        /// <summary>
        /// Get the Order with the given UUID
        /// </summary>
        /// <param name="uuid"></param>
        /// <returns></returns>
        [HttpGet("{uuid}", Name = "GetOrders")]
        public Orders Get(string uuid)
        {
            return DatabaseService<Orders>.GetSingle(true, o => o.Uuid.Equals(uuid));
        }

        /// <summary>
        /// Apply a filter on the request
        /// </summary>
        /// <param name="uuid"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("filtered-list")]
        public List<Orders> GetFilteredList([FromBody] KeyValuePair<string, object> filter)
        {
            Type type = typeof(Orders);
            PropertyInfo pi = type.GetProperty(filter.Key);
            return DatabaseService<Orders>.GetList(true, a => pi.GetValue(a).Equals(filter.Value)).ToList();
        }

        /// <summary>
        /// Create an order
        /// </summary>
        /// <param name="order"></param>
        [HttpPost]
        public string Post([FromBody] Orders order)
        {
            if (string.IsNullOrEmpty(order.Uuid))
            {
                order.Uuid = Guid.NewGuid().ToString();
                DatabaseService<Orders>.Add(order);
            }
            return order.Uuid;
        }

        /// <summary>
        /// Update an Order
        /// </summary>
        /// <param name="order"></param>
        [HttpPut]
        public void Put([FromBody] Orders order)
        {
            order.IdOrders = DatabaseService<Orders>.GetSingle(false, c => c.Uuid.Equals(order.Uuid)).IdOrders;
            DatabaseService<Orders>.Update(order);
        }

        /// <summary>
        /// Delete an Order
        /// </summary>
        /// <param name="order"></param>
        [HttpDelete("{uuid}")]
        public void Delete(string uuid)
        {
            Orders order = DatabaseService<Orders>.GetSingle(false, c => c.Uuid.Equals(uuid));
            DatabaseService<Orders>.Remove(order);
        }
    }
}
