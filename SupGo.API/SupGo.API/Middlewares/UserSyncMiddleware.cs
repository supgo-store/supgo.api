﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SupGo.DAL.NetCore.Models;
using SupGo.DAL.NetCore.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web.Http;

namespace SupGo.API.Middlewares
{
    /// <summary>
    /// User synchronization between Keycloak and SupGo User table
    /// </summary>
    public class UserSyncMiddleware
    {
        private readonly RequestDelegate _next;

        public UserSyncMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            if(context.User.Claims.Any())
            {
                string keycloakUserUuid = context.User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value;
                User dbUser = DatabaseService<User>.GetSingle(false, u => u.Uuid.Equals(keycloakUserUuid));
                User kcUser = new User()
                {
                    Uuid = keycloakUserUuid,
                    Email = context.User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email).Value,
                    Firstname = context.User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.GivenName).Value,
                    Lastname = context.User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Surname).Value
                };

                // If user doesn't exist in database
                if (dbUser == null)
                {
                    // Create it
                    DatabaseService<User>.Add(kcUser);
                }
                else // If it does
                {
                    // Update it
                    dbUser.Firstname = kcUser.Firstname;
                    dbUser.Lastname = kcUser.Lastname;
                    dbUser.Email = kcUser.Email;

                    DatabaseService<User>.Update(dbUser);
                }
            }

            await _next(context);
        }
    }

    public static class UserSyncMiddlewareExtensions
    {
        public static IApplicationBuilder UseUserSync(
            this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<UserSyncMiddleware>();
        }
    }
}
