﻿using SupGo.DAL.NetCore.Models;
using SupGo.DAL.NetCore.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SupGo.API.Services
{
    public static class StockService
    {
        public static void InitializeStock(Product product)
        {
            List<Store> stores = DatabaseService<Store>.GetAll(true).ToList();
            foreach(Store store in stores)
            {
                DatabaseService<Stock>.Add(new Stock
                {
                    Uuid = Guid.NewGuid().ToString(),
                    ProductUuid = product.Uuid,
                    StoreUuid = store.Uuid,
                    Quantity = 0
                });
            }
        }
    }
}
